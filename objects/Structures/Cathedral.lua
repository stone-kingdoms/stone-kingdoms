local Structure = require("objects.Structure")
local Object = require("objects.Object")
local NotEnoughWorkersFloat = require("objects.Floats.NotEnoughWorkersFloat")

local tiles, quadArray = _G.indexBuildingQuads("church_large", true)
local CathedralAlias = _G.class("CathedralAlias", Structure)
function CathedralAlias:initialize(tile, gx, gy, parent, offsetY, offsetX)
    local mytype = "Static structure"
    self.parent = parent
    Structure.initialize(self, gx, gy, mytype)
    _G.state.map:setWalkable(self.gx, self.gy, 1)
    self.tile = tile
    self.baseOffsetY = offsetY or 0
    self.additionalOffsetY = 0
    self.offsetX = offsetX or 0
    self.offsetY = self.additionalOffsetY - self.baseOffsetY
    Structure.render(self)
end

function CathedralAlias:serialize()
    local data = {}
    local structData = Structure.serialize(self)
    for k, v in pairs(structData) do
        if type(v) ~= "function" and type(v) ~= "userdata" then
            data[k] = v
        end
    end
    data.tileKey = self.tileKey
    data.baseOffsetY = self.baseOffsetY
    data.additionalOffsetY = self.additionalOffsetY
    data.offsetX = self.offsetX
    data.offsetY = self.offsetY
    data.parent = _G.state:serializeObject(self.parent)
    return data
end

function CathedralAlias.static:deserialize(data)
    local obj = self:allocate()
    Object.deserialize(obj, data)
    Structure.load(obj, data)
    obj.parent = _G.state:dereferenceObject(data.parent)
    if data.tileKey then
        obj.tile = quadArray[data.tileKey]
        obj.tileKey = data.tileKey
        obj:render()
    end
    return obj
end

local cathedralFx = {
    ["Cathedral"] = { _G.fx["cathedral_01"] }
}

local Cathedral = _G.class("Cathedral", Structure)

Cathedral.static.WIDTH = 13
Cathedral.static.LENGTH = 13
Cathedral.static.HEIGHT = 17
Cathedral.static.DESTRUCTIBLE = true

function Cathedral:initialize(gx, gy)
    _G.JobController:add("Priest", self)
    Structure.initialize(self, gx, gy, "Cathedral")
    self.animated = false
    _G.state.map:setWalkable(self.gx, self.gy, 1)
    self.health = 50
    self.tile = quadArray[tiles + 1]
    self.offsetX = 0
    self.offsetY = -194
    self.freeSpots = 1
    self.worker = nil

    for tile = 1, tiles do
        local hsl = CathedralAlias:new(quadArray[tile], self.gx, self.gy + (tiles - tile + 1), self,
            -self.offsetY + 8 * (tiles - tile + 1))
        hsl.tileKey = tile
    end
    for tile = 1, tiles do
        local hsl = CathedralAlias:new(quadArray[tiles + 1 + tile], self.gx + tile, self.gy, self, -self.offsetY + 8 * tile
        , 16)
        hsl.tileKey = tiles + 1 + tile
    end
    local tileQuads = require("objects.object_quads")
    for xx = 0, Cathedral.static.WIDTH - 1 do
        for yy = 0, Cathedral.static.LENGTH - 1 do
            if not _G.objectFromSubclassAtGlobal(self.gx + xx, self.gy + yy, Structure) then
                CathedralAlias:new(tileQuads["empty"], self.gx + xx, self.gy + yy, self, 0, 0)
            end
        end
    end
    for xx = -1, self.class.WIDTH do
        for yy = -1, self.class.LENGTH do
            _G.terrainSetTileAt(self.gx + xx, self.gy + yy, _G.terrainBiome.scarceGrass)
        end
    end
    self.freeArmySpots = _G.newAutotable(2)
    for xx = 1, 17 do
        for yy = 14, 17 do
            self.freeArmySpots[xx][yy] = _G.state.map:isWalkable(xx, yy)
        end
    end
    self:applyBuildingHeightMap()

    self.float = NotEnoughWorkersFloat:new(self.gx, self.gy, 0, -64)
end

function Cathedral:freeAllSpots()
    for xx = 1, 17 do
        for yy = 14, 17 do
            self.freeArmySpots[xx][yy] = _G.state.map:isWalkable(xx, yy)
        end
    end
end

function Cathedral:anyFreeArmySpots()
    for xx = 1, 17 do
        for yy = 14, 17 do
            if self.freeArmySpots[xx][yy] == true then
                return true
            end
        end
    end
    return false
end

function Cathedral:getNextFreeSpot(soldier)
    for xx = 1, 17 do
        for yy = 14, 17 do
            if self.freeArmySpots[xx][yy] == true then
                self.freeArmySpots[xx][yy] = soldier
                _G.soldiers = _G.soldiers + 1
                return self.gx + xx, self.gy + yy, "south"
            end
        end
    end
    self:freeAllSpots()
    return self:getNextFreeSpot(soldier)
end

function Cathedral:destroy()
    _G.JobController:remove("Priest", self)
    self.float:destroy()
    if self.worker then
        self.worker:quitJob()
    end
    Structure.destroy(self)
end

function Cathedral:work(worker)
    worker.gx = self.gx + 3
    worker.gy = self.gy + 6
    worker:jobUpdate()
end

function Cathedral:leave(sleepInsteadOfLeaving)
    if self.worker then
        _G.JobController:add("Priest", self)
        if sleepInsteadOfLeaving then
            self.worker:quitJob()
        else
            self.worker:leaveVillage()
        end
        self.worker = nil
        self.freeSpots = 1
        self.float:activate(sleepInsteadOfLeaving)
        return true
    end
end

function Cathedral:join(worker)
    if self.health == -1 then
        _G.JobController:remove("Priest", self)
        worker:quitJob()
        return
    end
    if self.freeSpots == 1 then
        self.worker = worker
        worker.workplace = self
        self.freeSpots = self.freeSpots - 1
    end
    if self.freeSpots == 0 then
        self.float:deactivate()
    end
end

function Cathedral:exitTowardsStockpile()
    self:findExitPointTo("Stockpile", function(found, path)
        if found then
            self:sendWorkerToBlessBuilding()
        else
            print("No path found to stockpile!")
        end
    end)
end

function Cathedral:sendWorkerToBlessBuilding()
    if self.worker then
        self:respawnWorker(self.worker, "Looking to bless")
        self.worker:onExitPointFound()
    end
end

function Cathedral:onClick()
    local ActionBar = require("states.ui.ActionBar")
    ActionBar:switchMode("religion")
    ActionBar:showCathedralOptions(true)
    _G.selectedRecruitLocation = self
    _G.playSfx(self, cathedralFx["Cathedral"], true, 1)
end

function Cathedral:load(data)
    Object.deserialize(self, data)
    Structure.load(self, data)
    if data.worker then
        self.worker = _G.state:dereferenceObject(data.worker)
        self.worker.workplace = self
    end
    self.tile = quadArray[tiles + 1]
    Structure.render(self)
end

function Cathedral:serialize()
    local data = {}
    local structData = Structure.serialize(self)
    for k, v in pairs(structData) do
        if type(v) ~= "function" and type(v) ~= "userdata" then
            data[k] = v
        end
    end
    data.health = self.health
    data.offsetX = self.offsetX
    data.offsetY = self.offsetY
    data.freeSpots = self.freeSpots
    if self.worker then
        data.worker = _G.state:serializeObject(self.worker)
    end
    return data
end

function Cathedral.static:deserialize(data)
    local obj = self:allocate()
    obj:load(data)
    return obj
end

return Cathedral

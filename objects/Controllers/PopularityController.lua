local actionBar = require("states.ui.ActionBar")
local RationController = require("objects.Controllers.RationController")
local Events = require "objects.Enums.Events"

local function clamp(x, min, max)
    return x < min and min or (x > max and max or x)
end

local PopularityController = _G.class("PopularityController")
PopularityController.static.POPULARITY_INTERVAL = 3
function PopularityController:initialize()
    self.timer = self.class.POPULARITY_INTERVAL
    self.moodFoodFactor = RationController.moodFactor
    self.moodTaxFactor = _G.TaxController.moodFactor
    self.speedPopModifier = 2
    self.previousSpeedPopModifier = 2
    self.effects = {
        tax = 0,
        rations = 0,
        positiveBuildings = 0,
        ale = 0,
        religion = 0
    }
end

function PopularityController:serialize()
    local data = {}

    data.moodFoodFactor = self.moodFoodFactor
    data.moodTaxFactor = self.moodTaxFactor

    return data
end

function PopularityController:deserialize(data)
    for k, v in pairs(data) do
        self[k] = v
    end
    self.effects = {
        tax = 0,
        rations = 0,
        positiveBuildings = 0,
        ale = 0,
        religion = 0
    }
    self.timer = self.class.POPULARITY_INTERVAL
end

function PopularityController:calculatePositiveBuildingPopularity()
    local positiveBuildingFactor = _G.BuildingManager:getCountOfPositiveBuildings() - math.abs((math.ceil(_G.state.population / 16) - 1))
    if positiveBuildingFactor > 5 then
        positiveBuildingFactor = 5
    elseif positiveBuildingFactor < 0 then
        positiveBuildingFactor = 0
    end
    return positiveBuildingFactor
end

function PopularityController:update()
    local popularityOldValue = _G.state.popularity
    if not _G.campfireFloatPop then return end
    self.timer = self.timer + _G.dt
    if self.timer >= self.class.POPULARITY_INTERVAL then
        local tax = _G.TaxController:getMoodFactor()
        local rations = RationController:getMoodLevel()
        local _, ale = _G.AleController:getAleCoverageStats()
        local _, religion = _G.ReligionController:getCoverageStats()
        local positiveBuildings = math.floor(self:calculatePositiveBuildingPopularity())
        local rate = tax + rations + positiveBuildings + ale + religion
        self.effects["tax"] = tax
        self.effects["rations"] = rations
        self.effects["positiveBuildings"] = positiveBuildings
        self.effects["ale"] = ale
        self.effects["religion"] = religion
        _G.state.popularity = clamp(_G.state.popularity + rate, 0, 100)
        if popularityOldValue ~= _G.state.popularity then
            _G.bus.emit(Events.OnPopulationChange, popularityOldValue, _G.state.popularity)
        end
        actionBar:updatePopularityCount()
        self.timer = 0
        local x = _G.state.popularity
        if _G.state.popularity >= 50 then
            self.speedPopModifier = ((100 - x) / 25) * ((100 - x) / 25) * ((100 - x) / 25) * 0.08 + 0.09
        else
            self.speedPopModifier = ((x) / 50) * ((x) / 10) * 0.08 + 0.1
        end
        if self.previousSpeedPopModifier ~= self.speedPopModifier then
            _G.campfireFloatPop:updateSpeed(self.speedPopModifier)
        end
        self.previousSpeedPopModifier = self.speedPopModifier
    end
end

return PopularityController:new()
